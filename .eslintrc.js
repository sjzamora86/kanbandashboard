module.exports = {
    "parser": "babel-eslint",
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "no-unused-vars": 0,
        "no-undef": 0,
        "no-console": 0,
        "no-case-declarations": 0,
        "indent": ["error", 4, {SwitchCase: 1}],
        "semi": ["error", "always"],
        "arrow-spacing": 2,
        "keyword-spacing": ["error", { "before": true, "after": true }],
        "space-infix-ops": ["error", {"int32Hint": false}]
    }
};