import {ApiConstants} from 'src/lib/api-contants';

export const AppConfig = {
    // App Details
    appName: 'Release Dashboard',
    pageSizePerRequest: 50,
    urls: {
        api: ''
    },
    DEV: __DEV__,
    // API paths
    endpoints: {
        summary: {
            [ApiConstants.API_METHODS.POST]: '/api/summary'
        }
    }
};