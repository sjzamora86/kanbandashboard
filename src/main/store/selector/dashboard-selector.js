import {createSelector} from 'reselect';
import * as R from 'ramda';
import {timeAgo} from 'src/helpers/date-helper';
import {DashboardStatus} from 'src/components/common/dashboard-constants';

export const dashboardSelector = state => R.pathOr({}, ['dashboard'], state);

export const getDashboardData = createSelector(
    dashboardSelector,
    dashboard => {
        if (!dashboard.data) return dashboard;
        const blockedItems = R.filter(x => x.status === DashboardStatus.Blocked)(R.concat(dashboard.data.releases, dashboard.data.tasks));
        const plannedItems = R.filter(x => x.status === DashboardStatus.Planned)(R.concat(dashboard.data.releases, dashboard.data.tasks));
        const failedItems = R.filter(x => x.status === DashboardStatus.Failed)(R.concat(dashboard.data.releases, dashboard.data.tasks));
        const succeededItems = R.filter(x => x.status === DashboardStatus.Succeeded)(dashboard.data.tasks);
        const releaseCompleted = R.reject(x => x.status !== DashboardStatus.Succeeded)(dashboard.data.releases);
        const effortInHoursData = R.pluck('effortInHours')(releaseCompleted);
        const averageReleaseTime = R.pipe(R.sum, R.divide(R.__, effortInHoursData.length), x => x.toFixed(2))(effortInHoursData);
        const formatItem = release => {
            return {
                main: release.name,
                sub: timeAgo(release.effortInHours),
                status: release.status
            };
        };
        return {
            blockedItems: R.map(formatItem, blockedItems),
            plannedItems: R.map(formatItem, plannedItems),
            failedItems: R.map(formatItem, failedItems),
            succeededItems: R.map(formatItem, succeededItems),
            averageReleaseTime: averageReleaseTime,
            noOfReleases: effortInHoursData.length,
            sequenceCounts: R.map(release => {
                return {
                    main: release.name,
                    sub: release.numberOfSequence
                };
            }, releaseCompleted),
            releaseChart: {
                labels: R.pluck('name')(releaseCompleted),
                datasets: [{
                    label: 'Effort in Hours',
                    backgroundColor: 'rgba(54,185,223,0.5)',
                    borderColor: 'rgba(54,185,223,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(54,185,223,0.7)',
                    hoverBorderColor: 'rgba(54,185,223,0,8)',
                    data: effortInHoursData
                }]
            }
        };
    }
);