import * as MainActionTypes from 'src/main/store/actions/main/main-action-type';

export default (state = {
    error: null,
    isErrorPopupOpen: false
}, action) => {
    switch (action.type) {
        case MainActionTypes.SERVER_EXCEPTION_ERROR:
            return {
                ...state,
                error: action.error,
                isErrorPopupOpen: true
            };
        case MainActionTypes.CLEAR_SERVER_EXCEPTION_ERROR:
            return {
                ...state,
                error: null,
                isErrorPopupOpen: false
            };
        default:
            return state;
    }
};
