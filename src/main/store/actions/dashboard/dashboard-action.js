import { dispatchGetAsync } from 'src/lib/common/common-action-dispatchers';
import * as DashboardApi from 'src/api/dashboard-api';
import {DASHBOARD_PREFIX} from 'src/main/store/actions/dashboard/dashboard-action-prefix';

export function getSummaryFromServer() {
    return dispatchGetAsync(DASHBOARD_PREFIX, () => DashboardApi.getSummary());
}