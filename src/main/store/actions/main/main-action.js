import * as MainActionTypes from 'src/main/store/actions/main/main-action-type';


export function serverExceptionError(error) {
    return (dispatch) => {
        dispatch(serverExceptionErrorAction(error));
    };
}

export function clearExceptionError() {
    return (dispatch) => {
        dispatch({
            type: MainActionTypes.CLEAR_SERVER_EXCEPTION_ERROR
        });
    };
}

export function serverExceptionErrorAction(error) {
    return {
        type: MainActionTypes.SERVER_EXCEPTION_ERROR,
        error: error
    };
}
