import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import app from 'src/main/store/reducers/main-reducer';
import reduxThunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {commonReducer} from 'src/lib/common/common-reducer';
import {DASHBOARD_PREFIX} from 'src/main/store/actions/dashboard/dashboard-action-prefix';

export function configureStore() {

    let middleware = [reduxThunk];

    if (__DEV__) {
        // Dev-only middleware
        middleware = [
            ...middleware,
            createLogger(), // Logs state changes to the dev console
        ];
    }

    const reducers = combineReducers(
        {
            app,
            dashboard: commonReducer(DASHBOARD_PREFIX)
        }
    );
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const store = createStore(
        reducers,
        {}, // second argument overrides the initial state
        composeEnhancers(
            applyMiddleware(...middleware)
        )
    );

    return store;

}