import React, {Component, lazy, Suspense} from 'react';
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';
import PageLoader from 'src/components/common/PageLoader';
import BasePage from 'src/components/common/BasePage';
import {bindActionCreators} from 'redux';
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import {connect} from 'react-redux';
import * as AppAction from 'src/main/store/actions/main/main-action';
import ErrorDialog from 'src/components/common/ErrorDialog';

const DashboardContainer = withRouter(lazy(() => import('src/components/pages/DashboardContainer')));

class DefaultRoutes extends Component {
    onErrorDialogClose = () => {
        this.props.appActions.clearExceptionError();
    };

    render() {
        const {location, isErrorPopupOpen} = this.props;

        const currentKey = location.pathname.split('/')[1] || '/';
        const timeout = {enter: 500, exit: 500};
        const animationName = 'rag-fadeIn';

        return (
            <BasePage>
                <ErrorDialog isOpen={isErrorPopupOpen} onClose={this.onErrorDialogClose} />
                <TransitionGroup>
                    <CSSTransition key={currentKey} timeout={timeout} classNames={animationName} exit={false}>
                        <div>
                            <Suspense fallback={<PageLoader/>}>
                                <Switch location={location}>
                                    <Route path="/" component={DashboardContainer}/>
                                </Switch>
                            </Suspense>
                        </div>
                    </CSSTransition>
                </TransitionGroup>
            </BasePage>
        );
    }
}

const mapStateToProps = state => ({
    isErrorPopupOpen: state.app.isErrorPopupOpen
});

const mapDispatchToProps = dispatch => {
    return {
        appActions: bindActionCreators(AppAction, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DefaultRoutes);