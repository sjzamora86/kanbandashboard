import {withRouter} from 'react-router-dom';
import React, {Component} from 'react';
import DefaultRoutes from 'src/main/routes/DefaultRoutes';
import MainPageWrapper from 'src/components/common/BasePage';

class AppRoutes extends Component {
    render() {
        const {location} = this.props;
        return <MainPageWrapper>
            <DefaultRoutes location={location}/>
        </MainPageWrapper>;
    }
}


export default withRouter(AppRoutes);