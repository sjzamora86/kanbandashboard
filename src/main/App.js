import React, {Component} from 'react';
import {BrowserRouter} from 'react-router-dom';
// App Routes
// Vendor dependencies
import "src/main/Vendor";
// Application Styles
import 'scss/sb-admin-2.scss';
import 'scss/app.scss';
import AppRoutes from 'src/main/routes/AppRoutes';

class App extends Component {

    render() {

        // specify base href from env variable 'PUBLIC_URL'
        // use only if application isn't served from the root
        // for development it is forced to root only
        /* global PUBLIC_URL */
        const basename = process.env.NODE_ENV === 'development' ? '/' : (PUBLIC_URL || '/');

        return (
            <BrowserRouter basename={basename}>
                <AppRoutes />
            </BrowserRouter>
        );

    }
}

export default App;
