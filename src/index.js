import React from 'react';
import ReactDOM from 'react-dom';
import {configureStore} from 'src/main/store/app-store';
import { Provider } from 'react-redux';
import App from 'src/main/App';

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);