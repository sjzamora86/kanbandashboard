export const ApiConstants = {
    HTTP_METHODS: {
        GET: 'GET',
        POST: 'POST',
        PUT: 'PUT',
        DELETE: 'DELETE'
    },
    API_METHODS: {
        GET: 'get',
        GET_ITEM: 'getItem',
        GET_ALL: 'getAll',
        POST: 'post',
        PUT: 'put',
        DELETE: 'delete'
    }
};

export const ApiMethodsMapping = {
    [ApiConstants.API_METHODS.GET]: ApiConstants.HTTP_METHODS.GET,
    [ApiConstants.API_METHODS.GET_ALL]: ApiConstants.HTTP_METHODS.GET,
    [ApiConstants.API_METHODS.GET_ITEM]: ApiConstants.HTTP_METHODS.GET,
    [ApiConstants.API_METHODS.POST]: ApiConstants.HTTP_METHODS.POST,
    [ApiConstants.API_METHODS.PUT]: ApiConstants.HTTP_METHODS.PUT,
    [ApiConstants.API_METHODS.DELETE]: ApiConstants.HTTP_METHODS.DELETE
};