import * as R from 'ramda';
import {AppConfig} from 'src/config/app-config';
import {formatUrl} from 'src/helpers/url-helper';
import {debug} from 'src/helpers/debug';
import {ApiMethodsMapping} from 'src/lib/api-contants';

const fetcher = (method, endpoint, params = {}, payload, contentType) => {

    function composeSuccessResponse(data) {
        return {
            success: true,
            data: data
        };
    }

    function composeFailedResponse(error) {
        return {
            success: false,
            error: error
        };
    }

    return new Promise((resolve, reject) => {
        let req = {
            method: method
        };

        if (contentType === 'application/json') {
            req.headers = {
                'Content-Type': contentType,
                'Accept': contentType
            };

        }

        req = R.isNil(payload) ? req : R.mergeDeepRight(req, {
            body: contentType === 'application/json' ? JSON.stringify(payload) : payload
        });

        endpoint = formatUrl(endpoint, params);
        debug(`API fetcher endpoint: ${endpoint}`, req);

        fetch(endpoint, req)
            .then(response => {
                debug(`API fetcher data before json: ${endpoint}`, response);
                return response.json()
                    .then((obj) => {
                        return {
                            code: response.status,
                            data: obj
                        };
                    });
            })
            .then(json => {
                debug(`API fetcher response json: ${endpoint}`, json);
                if (!R.equals(json.code, 200)) {
                    return resolve(composeFailedResponse(new Error(json.data.message)));
                }
                return resolve(composeSuccessResponse(json.data.data));
            })
            .catch((err) => {
                //TODO: global error messaging
                debug(`API fetcher error: $error: ${JSON.stringify(err)}`);
                return reject(err);
            });
    });
};

/**
 * Build services from Endpoints
 * - So we can call AppAPI.posts.get() for example
 */
const assocFn = (endpoint) => {
    return R.reduce((acc, value) => {
        const isObject = R.is(Object, endpoint[value]);
        const fullAPIEndpoint = R.concat(AppConfig.urls.api, isObject ? endpoint[value].endpoint : endpoint[value]);
        const contentType = isObject ? endpoint[value].contentType : 'application/json';
        acc = R.assoc(value, (params, payload) => fetcher(ApiMethodsMapping[value], fullAPIEndpoint, params, payload, contentType))(acc);
        return acc;
    }, {})(R.keys(endpoint));
};
const AppAPI = R.map(assocFn)(AppConfig.endpoints);
export default AppAPI;