import {commonMethods, commonStages} from 'src/lib/common/common-action-suffixes';
import {serverExceptionErrorAction} from 'src/main/store/actions/main/main-action';

export function dispatchInitialise(prefix) {
    return (dispatch) => {
        dispatch({
            name: prefix,
            type: `${prefix}${commonMethods.INITIALISE}${commonStages.BEGIN}`
        });
    };
}

export function dispatchUpdateItem(prefix, data) {
    return (dispatch) => {
        dispatch(createDispatchSuccess(prefix, commonMethods.UPDATE_ITEM, data));
    };
}

export function dispatchGetAsync(prefix, fn) {
    return (dispatch) => {
        dispatch(createDispatchBegin(prefix, commonMethods.GET));
        return new Promise((resolve, reject) => {
            fn()
                .then((response) => {
                    if (!response.success) {
                        dispatch(createDispatchError(prefix, commonMethods.GET, response.error));
                        dispatch(serverExceptionErrorAction(response.error));
                        return reject(response.error);
                    }
                    dispatch(createDispatchSuccess(prefix, commonMethods.GET, response.data));
                    return resolve(response.data);
                })
                .catch((err) => {
                    dispatch(createDispatchError(prefix, commonMethods.GET, err));
                    dispatch(serverExceptionErrorAction(err));
                    return reject(err);
                });
        });
    };
}

export function dispatchPostAsync(prefix, fn) {
    return (dispatch) => {
        dispatch(createDispatchBegin(prefix, commonMethods.POST));
        return new Promise((resolve, reject) => {
            fn()
                .then((response) => {
                    if (!response.success) {
                        dispatch(createDispatchError(prefix, commonMethods.POST, response.error));
                        dispatch(serverExceptionErrorAction(response.error));
                        return reject(response.error);
                    }
                    dispatch(createDispatchSuccess(prefix, commonMethods.POST, response.data));
                    return resolve(response.data);
                })
                .catch((err) => {
                    dispatch(createDispatchError(prefix, commonMethods.POST, err));
                    return reject(err);
                });
        });
    };
}

export function dispatchPutAsync(prefix, fn) {
    return (dispatch) => {
        dispatch(createDispatchBegin(prefix, commonMethods.PUT));
        return new Promise((resolve, reject) => {
            fn()
                .then((response) => {
                    if (!response.success) {
                        dispatch(createDispatchError(prefix, commonMethods.PUT, response.error));
                        dispatch(serverExceptionErrorAction(response.error));
                        return reject(response.error);
                    }
                    dispatch(createDispatchSuccess(prefix, commonMethods.PUT, response.data));
                    return resolve(response.data);
                })
                .catch((err) => {
                    dispatch(createDispatchError(prefix, commonMethods.PUT, err));
                    return reject(err);
                });
        });
    };
}

export function dispatchDeleteAsync(prefix, fn) {
    return (dispatch) => {
        dispatch(createDispatchBegin(prefix, commonMethods.DELETE));
        return new Promise((resolve, reject) => {
            fn()
                .then((response) => {
                    if (!response.success) {
                        dispatch(createDispatchError(prefix, commonMethods.DELETE, response.error));
                        dispatch(serverExceptionErrorAction(response.error));
                        return reject(response.error);
                    }
                    dispatch(createDispatchSuccess(prefix, commonMethods.DELETE, response.data));
                    return resolve(response.data);
                })
                .catch((err) => {
                    dispatch(createDispatchError(prefix, commonMethods.DELETE, err));
                    return reject(err);
                });
        });
    };
}

function createDispatchBegin(prefix, method) {
    return {
        name: prefix,
        type: `${prefix}${method}${commonStages.BEGIN}`
    };
}

function createDispatchSuccess(prefix, method, data) {
    return {
        name: prefix,
        type: `${prefix}${method}${commonStages.END}`,
        data: data
    };
}

function createDispatchError(prefix, method, error) {
    return {
        name: prefix,
        type: `${prefix}${method}${commonStages.END_ERROR}`,
        error: error
    };
}