import {commonActionSuffixes} from 'src/lib/common/common-action-suffixes';

export function commonReducer(prefix, initialState = {
    isLoading: null,
    isError: null
}) {
    return (state = initialState, action) => {
        if (prefix !== action.name) {
            return state;
        }
        switch (action.type) {
            case `${prefix}${commonActionSuffixes.INITIALISE_BEGIN}`:
                return initialState;
            case `${prefix}${commonActionSuffixes.GET_BEGIN}`:
            case `${prefix}${commonActionSuffixes.POST_BEGIN}`:
            case `${prefix}${commonActionSuffixes.PUT_BEGIN}`:
            case `${prefix}${commonActionSuffixes.DELETE_BEGIN}`:
                return {
                    ...state,
                    isLoading: true,
                    isError: false,
                    isSuccess: undefined
                };
            case `${prefix}${commonActionSuffixes.GET_END}`:
                return {
                    ...state,
                    data: action.data,
                    dataPost: {},
                    isLoading: false,
                    isError: false,
                    isSuccess: undefined
                };
            case `${prefix}${commonActionSuffixes.POST_END}`:
            case `${prefix}${commonActionSuffixes.PUT_END}`:
                return {
                    ...state,
                    dataPost: action.data,
                    isLoading: false,
                    isError: false,
                    isSuccess: true
                };
            case `${prefix}${commonActionSuffixes.DELETE_END}`:
                return {
                    ...state,
                    isLoading: false,
                    isError: false,
                    isSuccess: true
                };
            case `${prefix}${commonActionSuffixes.GET_END_ERROR}`:
            case `${prefix}${commonActionSuffixes.POST_END_ERROR}`:
            case `${prefix}${commonActionSuffixes.PUT_END_ERROR}`:
            case `${prefix}${commonActionSuffixes.DELETE_END_ERROR}`:
                return {
                    ...state,
                    isLoading: false,
                    isError: true,
                    error: action.error,
                    isSuccess: false
                };
            case `${prefix}${commonActionSuffixes.UPDATE_ITEM_END}`:
                return {
                    ...state,
                    isSuccess: undefined,
                    data: action.data
                };
            default:
                return state;
        }
    };
}