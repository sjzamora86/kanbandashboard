export const commonMethods = {
    INITIALISE: "_INITIALISE",
    GET: "_GET",
    POST: "_POST",
    PUT: "_PUT",
    DELETE: "_DELETE",
    UPDATE_ITEM: "_UPDATE_ITEM"
};

export const commonStages = {
    BEGIN: "_BEGIN",
    END: "_END",
    END_ERROR: "_END_ERROR"
};

export const commonActionSuffixes = {
    INITIALISE_BEGIN: "_INITIALISE_BEGIN",
    GET_BEGIN: "_GET_BEGIN",
    POST_BEGIN: "_POST_BEGIN",
    PUT_BEGIN: "_PUT_BEGIN",
    DELETE_BEGIN: "_DELETE_BEGIN",
    INITIALISE_END: "_INITIALISE_BEGIN",
    GET_END: "_GET_END",
    POST_END: "_POST_END",
    PUT_END: "_PUT_END",
    DELETE_END: "_DELETE_END",
    UPDATE_ITEM_END: "_UPDATE_ITEM_END",
    GET_END_ERROR: "_GET_END_ERROR",
    POST_END_ERROR: "_POST_END_ERROR",
    PUT_END_ERROR: "_PUT_END_ERROR",
    DELETE_END_ERROR: "_DELETE_END_ERROR"
};