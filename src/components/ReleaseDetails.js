import React, {Component} from 'react';

class ReleaseDetails extends Component {

    render() {
        const {count} = this.props;
        return (
            <div className="average-release-time">
                <div className="text-center">
                    <div className="hours">{count}</div>
                    <p className="lead text-gray-800">{`Release${count > 1 ? 's' : ''}`}</p>
                </div>
            </div>
        );
    }
}

export default ReleaseDetails;