import React, {Component} from 'react';

class AverageHeader extends Component {

    render() {
        const {hours} = this.props;
        return (
            <div className="average-release-time">
                <div className="text-center">
                    <div className="hours">{hours}</div>
                    <p className="lead text-gray-800">{`Hour${hours > 1 ? 's' : ''}`}</p>
                </div>
            </div>
        );
    }
}

export default AverageHeader;