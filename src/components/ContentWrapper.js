import React, {Component} from 'react';

class ContentWrapper extends Component {

    render() {
        return (
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default ContentWrapper;