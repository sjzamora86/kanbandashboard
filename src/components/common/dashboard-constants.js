export const DashboardStatus = {
    Succeeded: 'Succeeded',
    Failed: 'Failed',
    Blocked: 'Blocked',
    Planned: 'Planned'
};