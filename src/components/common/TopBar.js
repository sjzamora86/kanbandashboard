import React from 'react';

const TopBar = props => {

    return <nav className="navbar navbar-expand bg-gradient-primary topbar mb-4 static-top shadow">
        <h1 className="h3 mb-0 text-white">BWB Release Dashboard</h1>

        <ul className="navbar-nav ml-auto">
            <li className="nav-item dropdown no-arrow">
                <span className="d-lg-inline text-white">{props.date}</span>
            </li>

        </ul>
    </nav>;

};

export default TopBar;