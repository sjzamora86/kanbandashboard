import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';

class ErrorDialog extends Component {

    render() {
        const {isOpen, onClose} = this.props;
        return  <Modal isOpen={isOpen} toggle={_ => onClose(false)}>
            <ModalHeader className="bg-danger" toggle={_ => onClose(false)}>Error</ModalHeader>
            <ModalBody>{`There's an unexpected server error. Please try again.`}</ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={_ => onClose(false)}>Close</Button>
            </ModalFooter>
        </Modal>;
    }
}

ErrorDialog.propTypes = {
    isOpen: PropTypes.bool,
    onClose: PropTypes.func,
};

export default ErrorDialog;