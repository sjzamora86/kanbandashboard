import React, {Component} from 'react';

class List extends Component {

    render() {
        const {collection, badgeClassName} = this.props;
        return (
            <div className="list-group mb0">
                {
                    collection && collection.map((item, index) => {
                        return <div className="list-tasks list-group-item list-group-item-action" key={`list-item-${index}`}>
                            <div className="media">
                                <div className="media-body">
                                    <p className="mb-1">
                                        {
                                            item.status && <em className={`fa-fw fas fa-circle mr-2 ${`status-${item.status.toLowerCase()}`}`}></em>
                                        }
                                        <span className="circle bg-success circle-lg text-left"></span>
                                        <span className="main">{item.main}</span>
                                    </p>
                                    {
                                        item.extra && <p className="mb-1 text-muted font-italic">{item.extra}</p>
                                    }

                                </div>
                                <div className="ml-auto">
                                    {
                                        item.sub && <span className={`badge ${badgeClassName || 'badge-primary'} float-right`}>{item.sub}</span>
                                    }
                                </div>
                            </div>
                        </div>;
                    })
                }
            </div>
        );
    }
}

export default List;