import React, {Component} from 'react';
import ContentWrapper from 'src/components/ContentWrapper';
import TopBar from 'src/components/common/TopBar';
import {Card, CardBody, CardHeader, Col, Row} from 'reactstrap';
import AverageHeader from 'src/components/AverageHeader';
import List from 'src/components/common/List';
import {HorizontalBar} from 'react-chartjs-2';
import {bindActionCreators} from 'redux';
import * as DashboardAction from 'src/main/store/actions/dashboard/dashboard-action';
import {connect} from 'react-redux';
import {getDashboardData} from 'src/main/store/selector/dashboard-selector';
import ReleaseDetails from 'src/components/ReleaseDetails';
import {formatDate} from 'src/helpers/date-helper';

const Bar = {
    type: 'horizontalBar',
    options: {
        legend: {
            display: false
        }
    }
};

class DashboardContainer extends Component {

    state = {
        loaded: false,
        currentTime: formatDate(new Date())
    };

    componentDidMount() {
        if (!this.state.loaded) {
            this.setState({loaded: true}, () => {
                this.props.dashboardActions.getSummaryFromServer();
            });
        }

        setInterval(() => {
            this.props.dashboardActions.getSummaryFromServer();
        }, 60000);

        setInterval(() => {
            this.setState({currentTime: formatDate(new Date())});
        }, 1000);
    }

    render() {
        const {dashboardData} = this.props;
        const {currentTime} = this.state;

        return <ContentWrapper>
            <TopBar date={currentTime} />
            <div className="container-fluid">
                <div className="row">

                    <div className="col-xl-3 col-md-6 mb-4">
                        <Card className="border-left-orange shadow h-100 py-2">
                            <CardBody>
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div
                                            className="text-xs font-weight-bold text-orange text-uppercase mb-1">Blocked
                                            Items
                                        </div>
                                        <div
                                            className="h5 mb-0 font-weight-bold text-gray-800">{dashboardData.blockedItems && dashboardData.blockedItems.length}</div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fas fa-ban fa-2x text-orange"></i>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <Card className="border-left-danger shadow h-100 py-2">
                            <CardBody>
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div className="text-xs font-weight-bold text-danger text-uppercase mb-1">Failed
                                            Items
                                        </div>
                                        <div
                                            className="h5 mb-0 font-weight-bold text-gray-800">{dashboardData.failedItems && dashboardData.failedItems.length}</div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fas fa-times fa-2x text-danger"></i>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <Card className="border-left-info shadow h-100 py-2">
                            <CardBody>
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div className="text-xs font-weight-bold text-info text-uppercase mb-1">Planned
                                            Items
                                        </div>
                                        <div
                                            className="h5 mb-0 font-weight-bold text-gray-800">{dashboardData.plannedItems && dashboardData.plannedItems.length}</div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fas fa-clipboard fa-2x text-info"></i>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-xl-3 col-md-6 mb-4">
                        <Card className="border-left-success shadow h-100 py-2">
                            <CardBody>
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div
                                            className="text-xs font-weight-bold text-success text-uppercase mb-1">Succeeded
                                            Items
                                        </div>
                                        <div
                                            className="h5 mb-0 font-weight-bold text-gray-800">{dashboardData.succeededItems && dashboardData.succeededItems.length}</div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fas fa-check-circle fa-2x text-success"></i>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>


                </div>

                <Row>
                    <Col lg="9">
                        <Card className="shadow">
                            <CardHeader>
                                <h6 className="m-0 font-weight-bold text-primary">Hours Spent In Release</h6>
                            </CardHeader>
                            <CardBody>
                                <HorizontalBar type="horizontalBar" data={dashboardData.releaseChart} options={Bar.options}/>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="3">
                        <Col md="12">
                            <Row md="12" className="pb-4">
                                <Card className="shadow col-md-12 p-0">
                                    <CardHeader>
                                        <h6 className="m-0 font-weight-bold text-primary">Average Release Time</h6>
                                    </CardHeader>
                                    <CardBody>
                                        <AverageHeader hours={dashboardData.averageReleaseTime}/>
                                    </CardBody>
                                </Card>
                            </Row>
                            <Row>
                                <Card className="shadow col-md-12 p-0">
                                    <CardHeader>
                                        <h6 className="m-0 font-weight-bold text-primary">Number of Releases</h6>
                                    </CardHeader>
                                    <CardBody>
                                        <ReleaseDetails count={dashboardData.noOfReleases}/>
                                    </CardBody>
                                </Card>
                            </Row>
                        </Col>

                    </Col>
                </Row>

                <Row className="pt-4">
                    <Col>
                        <Card className="shadow mb-4">
                            <CardHeader>
                                <h6 className="m-0 font-weight-bold text-primary">Sequence Count Per Release</h6>
                            </CardHeader>
                            <CardBody>
                                <List collection={dashboardData.sequenceCounts}/>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row className="pt-4">
                    <Col lg={6}>
                        <Row>
                            <Col>
                                <Card className="shadow mb-4">
                                    <CardHeader>
                                        <h6 className="m-0 font-weight-bold text-primary">Blocked Items</h6>
                                    </CardHeader>
                                    <CardBody>
                                        <Row>
                                            <Col>
                                                <div className="ml-auto">
                                                    <div className="text-xs font-weight-bold text-uppercase float-left pl-5 pb-2">Name</div>
                                                    <div className="text-xs font-weight-bold text-uppercase float-right pr-4 pb-2">blocked since</div>
                                                </div>
                                            </Col>
                                        </Row>
                                        <List collection={dashboardData.blockedItems}/>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Col>

                    <Col lg={6}>
                        <Row>
                            <Col>
                                <Card className="shadow mb-4">
                                    <CardHeader>
                                        <h6 className="m-0 font-weight-bold text-primary">Failed Items</h6>
                                    </CardHeader>
                                    <CardBody>
                                        <Row>
                                            <Col>
                                                <div className="ml-auto">
                                                    <div className="text-xs font-weight-bold text-uppercase float-left pl-5 pb-2">Name</div>
                                                    <div className="text-xs font-weight-bold text-uppercase float-right pr-4 pb-2">time spent</div>
                                                </div>
                                            </Col>
                                        </Row>
                                        <List collection={dashboardData.failedItems}/>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="pt-4">
                    <Col lg={6}>
                        <Row>
                            <Col>
                                <Card className="shadow mb-4">
                                    <CardHeader>
                                        <h6 className="m-0 font-weight-bold text-primary">Planned Items</h6>
                                    </CardHeader>
                                    <CardBody>
                                        <List collection={dashboardData.plannedItems}/>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={6}>
                        <Row>
                            <Col>
                                <Card className="shadow mb-4">
                                    <CardHeader>
                                        <h6 className="m-0 font-weight-bold text-primary">Succeeded Items</h6>
                                    </CardHeader>
                                    <CardBody>
                                        <Row>
                                            <Col>
                                                <div className="ml-auto">
                                                    <div className="text-xs font-weight-bold text-uppercase float-left pl-5 pb-2">Name</div>
                                                    <div className="text-xs font-weight-bold text-uppercase float-right pr-4 pb-2">time spent</div>
                                                </div>
                                            </Col>
                                        </Row>
                                        <List collection={dashboardData.succeededItems}/>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        </ContentWrapper>;
    }
}

const mapStateToProps = state => ({
    dashboardData: getDashboardData(state)
});

const mapDispatchToProps = dispatch => {
    return {
        dashboardActions: bindActionCreators(DashboardAction, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);