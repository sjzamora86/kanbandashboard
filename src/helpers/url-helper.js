import * as R from 'ramda';

export function formatUrl(url, paramQuery) {
    return R.pipe(
        R.match(/\{(.*?)\}/g),
        R.reduce((acc, value) => {
            const actualValue = R.pathOr(value, [R.replace(/\{|\}/g , '', value)], paramQuery);
            const myRe = new RegExp(value, 'g');
            return R.replace(myRe, encodeURIComponent(actualValue), acc);
        }, url)
    )(url);
}