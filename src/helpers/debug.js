const DEBUG_MODE = __DEV__;

export function debug(message, obj) {
    if (DEBUG_MODE && (message || obj)) {
        console.log(message, obj);
        console.log('%c ...', 'color: #CCC');
    }
}