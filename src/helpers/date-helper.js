import moment from 'moment';
import * as R from 'ramda';

export function isInRange(date, start, end) {
    const compareDate = moment(date, 'DD-MM-YYYY');
    const startDate = moment(start, 'DD-MM-YYYY');
    const endDate = moment(end, 'DD-MM-YYYY');
    return compareDate.isBetween(startDate, endDate);
}

export function convertToMoment(date) {
    if (typeof date === 'string') {
        return moment(date);
    }
    return date;
}

export function formatDate(date) {
    return moment.parseZone(date).local().format("MMM DD YYYY h:mm:ss a");
}

export function formatShortDate(date) {
    return moment.parseZone(date).local().format("MMM DD YYYY HH:mm:ss");
}

export function subtractDay(date, noOfDays) {
    return moment(date).subtract(noOfDays, 'days');
}

export function simpleFormatDate(date, format = 'DD-MM-YYYY') {
    if (R.isNil(date) || R.isEmpty(date)) {
        return '';
    }
    return moment.parseZone(date).local().format(format);
}

export function daysInMonth(month, year) { // Use 1 for January, 2 for February, etc.
    return new Date(year, month, 0).getDate();
}

export function timeAgo(hours) {
    if (hours === 0) return null;
    if (hours < 24) return `${hours} hour${hours > 1 ? 's' : ''}`;
    const days = Math.floor(hours / 24);
    const hoursRemaining = hours - (days * 24);
    if (hoursRemaining > 0) return `${days} day${days > 1 ? 's' : ''} ${hoursRemaining} hour${hoursRemaining > 1 ? 's' : ''}`;
    return `${days} day${days > 1 ? 's' : ''}`;
}