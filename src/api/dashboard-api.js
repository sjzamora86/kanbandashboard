import AppAPI from 'src/lib/api';

export function getSummary() {
    return Promise.resolve({
        success: true,
        data: {
            "tasks": [{
                "name": "Activate FeatureFlag  PricingAdmin-EditBusinessRates for Pricing",
                "numberOfSequence": 0,
                "effortInHours": 2,
                "status": "Succeeded"
            }, {
                "name": "Datapower call fails on opportunities ",
                "numberOfSequence": 0,
                "effortInHours": 838,
                "status": "Blocked"
            }, {
                "name": "Account Service Failure",
                "numberOfSequence": 0,
                "effortInHours": 1,
                "status": "Failed"
            }, {
                "name": "SIT CSA service returns 500 on get credit-sales API",
                "numberOfSequence": 0,
                "effortInHours": 1,
                "status": "Succeeded"
            }, {
                "name": "BWB Feature Activation - Tax Details 17/07/2019",
                "numberOfSequence": 0,
                "effortInHours": 3,
                "status": "Succeeded"
            }, {
                "name": "BWB Union and Rehearsal failed to authenticate remote cert",
                "numberOfSequence": 0,
                "effortInHours": 5,
                "status": "Failed"
            }, {"name": "SQL failure on master branch", "numberOfSequence": 0, "effortInHours": 24, "status": "Blocked"}],
            "releases": [{
                "name": "BWB Weekly Release - week ending 26/07/2019",
                "numberOfSequence": 3,
                "effortInHours": 15,
                "status": "Succeeded"
            }, {
                "name": "BWB Pipeline Board Test1 - Release Tax-ph1",
                "numberOfSequence": 1,
                "effortInHours": 0,
                "status": "Planned"
            }, {
                "name": "BWB Weekly Release - week ending 16/08/2019",
                "numberOfSequence": 2,
                "effortInHours": 8,
                "status": "Succeeded"
            }, {
                "name": "BWB Weekly Release - week ending 09/08/2019",
                "numberOfSequence": 1,
                "effortInHours": 8,
                "status": "Succeeded"
            }, {
                "name": "BWB Weekly Release - week ending 12/07/2019",
                "numberOfSequence": 9,
                "effortInHours": 146,
                "status": "Succeeded"
            }, {
                "name": "BWB Weekly Release - week ending 28/06/2019",
                "numberOfSequence": 0,
                "effortInHours": 98,
                "status": "Succeeded"
            }, {
                "name": "BWB Weekly Release - week ending 21/06/2019",
                "numberOfSequence": 3,
                "effortInHours": 24,
                "status": "Succeeded"
            }, {
                "name": "Pricing Tool MS Deployment - Docker image certificate update",
                "numberOfSequence": 1,
                "effortInHours": 75,
                "status": "Succeeded"
            }, {
                "name": "BWB Weekly Release - week ending 02/08/2019",
                "numberOfSequence": 3,
                "effortInHours": 178,
                "status": "Succeeded"
            }]
        }
    });
    //return AppAPI.summary.get();
}
