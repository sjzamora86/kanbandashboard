const R = require('ramda');
const merge = require("webpack-merge");

const endpoints = [
    '/api'
];

const baseConfig = require('./webpack.base.config');

const devConfiguration = env => {
    return merge([
        {
            devServer: {
                historyApiFallback: true,
                port: 9898,
                disableHostCheck: true,
                proxy: R.reduce((acc, value) => {
                    acc[value] = {
                        target: 'http://localhost:9090',
                        secure: false
                    };
                    return acc;
                }, {})(endpoints)
            }
        }
    ]);
};

module.exports = env => {
    return merge(baseConfig(env), devConfiguration(env));
};