const path = require('path');
const webpack = require('webpack');
const merge = require("webpack-merge");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');

const APP_DIR = path.resolve(__dirname, '../src'); // <===== new stuff added here
const isDebug = !process.argv.includes('-p');

module.exports = env => {
    const { PLATFORM, VERSION } = env;
    const plugins = [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            inject: false,
            hash: true,
            cache: false,
            template: './template/index.ejs',
            locals: {}
        }),
        new webpack.DefinePlugin({
            'process.env.VERSION': JSON.stringify(env.VERSION),
            'process.env.PLATFORM': JSON.stringify(env.PLATFORM),
            'PUBLIC_URL': JSON.stringify(process.env.PUBLIC_URL),
            __DEV__: isDebug,
        }),
        new webpack.ProvidePlugin({
            'window.moment': 'moment',
            'moment': 'moment'
        })
    ];

    if (isDebug) {
        plugins.push(new DashboardPlugin());
    }

    return merge([
        {
            entry: ['@babel/polyfill', APP_DIR], // <===== new stuff added here
            devtool: isDebug ? 'cheap-module-eval-source-map' : 'source-map',
            module: {
                rules: [
                    {
                        test: /\.(js|jsx)$/,
                        exclude: /node_modules/,
                        use: ['babel-loader']
                    },
                    {
                        test: /\.js$/,
                        exclude: /node_modules/,
                        use: ['babel-loader', 'eslint-loader']
                    },
                    {
                        test: /\.hbs$/,
                        exclude: /node_modules/,
                        use: {
                            loader: 'handlebars-loader'
                        }
                    },
                    {
                        test: /\.(css|sass|scss)$/,
                        use: [
                            PLATFORM === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
                            'css-loader',
                            'sass-loader'
                        ]
                    },
                    {
                        test: /jquery\.flot\.resize\.js$/,
                        use: ['imports-loader?this=>window']
                    },
                    {
                        test: /\.(png|jpg|jpeg|gif|ico)$/,
                        use: [
                            {
                                // loader: 'url-loader'
                                loader: 'file-loader',
                                options: {
                                    name: './img/[name].[ext]'
                                }
                            }
                        ]
                    },
                    {
                        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                        loader: 'file-loader',
                        options: {
                            name: './fonts/[name].[hash].[ext]'
                        }
                    }
                ]
            },
            plugins: plugins,
            resolve: {
                extensions: ['.webpack.js', '.web.js', '.js'],
                modules: [
                    path.resolve('./'),
                    'node_modules'
                ]
            }
        }
    ]);
};
